<?php
session_start();
$data = json_decode(file_get_contents('php://input'));

$servername = "localhost:3306";
$username = "root";
$dbpassword = "boubacar";
$dbname = "portfolio";
$conn = mysqli_connect($servername, $username, $dbpassword, $dbname);
mysqli_select_db($conn, "portfolio");

$email = $data->email;
$password = $data->password;

$sql = "SELECT * FROM utilisateurs WHERE email='$email'";
$query = mysqli_query($conn, $sql);

if (!$query) {
    http_response_code(500);
    echo json_encode(array("message" => "Erreur de requête : " . mysqli_error($conn)));
} else {
    $rs = mysqli_fetch_assoc($query);
    $hashedPassword = $rs["password"];

    if ($rs["typeUtilisateur"] == "utilisateur" && password_verify($password, $hashedPassword)) {

        // Mettre à jour la date de dernière connexion
        $updateSql = "UPDATE utilisateurs SET dernierConnexion = NOW() WHERE email = '$email'";
        mysqli_query($conn, $updateSql);
        
        $_SESSION["id_utilisateur"] = $rs['typeUtilisateur'];

        http_response_code(200);
        $outp = json_encode(array(
            "id_utilisateur" => $rs['id_utilisateur'],
            "email" => $rs["email"],
            "prenom" => $rs["prenom"],
            "nom" => $rs["nom"],
            "Status" => "utilisateur",
        ));
        echo $outp;
    } else if ($rs["typeUtilisateur"] == "admin" && password_verify($password, $hashedPassword)) {
        echo json_encode(array("Status" => "admin"));
    } else {
        http_response_code(202);
        echo json_encode(array("Status" => "202"));
    }
}
?>
