<?php

$data = json_decode(file_get_contents('php://input'), true);

$servername = "localhost";
$username = "root";
$dbpassword = "boubacar";
$dbname = "portfolio";

$conn = mysqli_connect($servername, $username, $dbpassword, $dbname);

$nom = $data['nom'];
$prenom = $data['prenom'];
$email = $data['email'];
$password = $data['password'];
$passwordChiffre = password_hash($password, PASSWORD_DEFAULT);

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $response = array('status' => 'emailInvalide');
    echo json_encode($response);
    exit;
} else {
    $sql = "SELECT * FROM utilisateurs WHERE email = '$email'";
    $result = mysqli_query($conn, $sql);
    $emailUnique = mysqli_num_rows($result);
}

if ($emailUnique > 0) {

    $response=array('status'=>'invalide');
    echo json_encode($response);
    
} else if (!empty($nom) && !empty($prenom) && !empty($email) && !empty($password) ) {

    $query = "INSERT INTO utilisateurs (nom, prenom, email, password, dernierConnexion) VALUES ('$nom', '$prenom', '$email', '$passwordChiffre', NOW())";
    $result = mysqli_query($conn, $query);
    $response=array('status' => 'valide');
    echo json_encode($response);

}


mysqli_close($conn);
?>

