<?php
$data = json_decode(file_get_contents('php://input'), true);

$servername = "localhost";
$username = "root";
$dbpassword = "boubacar";
$dbname = "portfolio";
$conn = mysqli_connect($servername, $username, $dbpassword, $dbname);

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $dbpassword);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * FROM experiences ORDER BY `id` DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();


$experiences = $stmt->fetchAll(PDO::FETCH_ASSOC);
$experiences = mb_convert_encoding($experiences, 'UTF-8', 'UTF-8');
echo json_encode($experiences);


