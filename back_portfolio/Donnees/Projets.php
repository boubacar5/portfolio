<?php
$data = json_decode(file_get_contents('php://input'), true);
// file_put_contents('fichier/post_json.log', json_encode($data));

$servername = "localhost";
$username = "root";
$dbpassword = "boubacar";
$dbname = "portfolio";
$conn = mysqli_connect($servername, $username, $dbpassword, $dbname);

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $dbpassword);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * FROM projets ORDER BY `id` DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();

$Projets = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($Projets);
// var_dump($Projets);