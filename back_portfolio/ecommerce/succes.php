<?php
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: white;
            text-align: center;
        }

        h1 {
            color: black;
            padding: 20px;
            border-bottom: 3px solid green;
        }

        a {
            font-size:15px;
            display: inline-block;
            padding: 15px 14px;
            background-color: black;
            color: white;
            text-decoration: none;
            margin-top: 20px;
            border-radius:15px
        }

        a:hover {
            background-color: grey;
        }
    </style>
</head>
<body>
    <h1>Paiement Réussi!</h1>
    <a href='http://localhost:3000'>Retour sur le site</a>
</body>
</html>
