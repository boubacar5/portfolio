<?php
header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Headers: access"); 
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$data = json_decode(file_get_contents('php://input'));

$servername = "localhost";
$username = "root";
$dbpassword = "boubacar";
$dbname = "portfolio";

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $dbpassword);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT * FROM achats ORDER BY `id` DESC";
$stmt = $conn->prepare($sql);
$stmt->execute();

$achats = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo json_encode($achats);