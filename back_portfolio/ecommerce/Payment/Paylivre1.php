<?php
// session_start();
use Firebase\JWT\JWT;

require_once '../cle.php';
require_once '../vendor/autoload.php';
require_once '../TransictionDonnees/InsertTransiction.php';

\Stripe\Stripe::setApiKey($stripeSecretKey);
header('Content-Type: application/json');

$urlPortfolio = 'http://localhost:3000';
$urlPayementReussi = 'http://backporftolio/ecommerce/succes';

// $servername = "localhost";
// $username = "root";
// $dbpassword = "boubacar";
// $dbname = "portfolio";
// $conn = mysqli_connect($servername, $username, $dbpassword, $dbname);

// $id_utilisateur = $_COOKIE['id_utilisateur']; // Récupérer l'ID de l'utilisateur depuis le cookie
// $sql = "SELECT jeton FROM utilisateurs WHERE id_utilisateur = '$id_utilisateur'";
// $result = mysqli_query($conn, $sql);

// if (!$result) {
//     http_response_code(500);
//     echo json_encode(array("message" => "Erreur de requête : " . mysqli_error($conn)));
//     exit;
// }

// $row = mysqli_fetch_assoc($result);
// $jetonDeLaBaseDeDonnees = $row['jeton'];

// $_SESSION['jeton'] = $jetonDeLaBaseDeDonnees;

// var_dump($jetonDeLaBaseDeDonnees);
// die;


$checkout_session = \Stripe\Checkout\Session::create([
  'line_items' => array([
    'price_data' => [
      'product_data' => [
          'name' => 'Apprendre HTML & CSS',
          'images' => ['https://m.media-amazon.com/images/I/81NB-H7UhdL._AC_UF1000,1000_QL80_.jpg'],
          "description" =>'Le livre présente dans un premier temps le langage HTML5, permettant de structurer une page, de disposer les éléments visuels très précisément comme le fait un logiciel de mise en page ou de dessin',
      ],
      'currency' => 'eur',
      'unit_amount' => 1999,
  ],
  'quantity' => 1,
]),
  'metadata' => ['id_utilisateur' => $id_utilisateur],
  'client_reference_id' => $jetonDeLaBaseDeDonnees,
  'shipping_address_collection' => ['allowed_countries' => ['FR', 'IT', 'SN']],
  'mode' => 'payment',
  'success_url' => $urlPayementReussi . '?session_id={CHECKOUT_SESSION_ID}',
  'cancel_url' => $urlPortfolio . '?canceled=true',
]);

header("HTTP/1.1 303 See Other");
header("Location: " . $checkout_session->url);
?>
