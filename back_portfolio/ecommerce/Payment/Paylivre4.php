<?php
// 16:53 instale deja fais, def nako ndem nak
require_once '../cle.php';
require_once '../vendor/autoload.php';
require_once '../TransictionDonnees/InsertTransiction.php';

\Stripe\Stripe::setApiKey($stripeSecretKey);
header('Content-Type: application/json');

$urlPortfolio = 'http://localhost:3000';
$urlPayementReussi = 'http://backporftolio/ecommerce/succes.php';
// $erreurPayement = '';


$checkout_session = \Stripe\Checkout\Session::create([
  'line_items' => [[
    'price_data' => [
      'product_data' => [
          'name' => 'Apprendre à développer des applications web avec PHP et Symfony',
          'images' => ['https://static.fnac-static.com/multimedia/Images/FR/NR/19/27/c2/12723993/1507-1/tsp20200929133245/Apprendre-a-developper-des-applications-web-avec-PHP-et-Symfony.jpg'],
          "description" =>'Ce livre s\'adresse à toute personne qui souhaite disposer des connaissances nécessaires pour apprendre à développer des applications web avec PHP et le framework Symfony (en version 5 au moment de l\'écriture)'],
      'currency' => 'eur',
      'unit_amount' => 4999,
  ],
  'quantity' => 1,
  ],
  
],
  'shipping_address_collection' => ['allowed_countries' => ['FR', 'IT', 'SN']],
  'mode' => 'payment',
  'success_url' => $urlPayementReussi . '?success=true',
  'cancel_url' => $urlPortfolio . '?canceled=true',
]);

header("HTTP/1.1 303 See Other");
header("Location: " . $checkout_session->url);