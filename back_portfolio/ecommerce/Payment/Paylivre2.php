<?php
require_once '../cle.php';
require_once '../vendor/autoload.php';
require_once '../TransictionDonnees/InsertTransiction.php';


\Stripe\Stripe::setApiKey($stripeSecretKey);
header('Content-Type: application/json');

$urlPortfolio = 'http://localhost:3000';
$urlPayementReussi = 'http://backporftolio/ecommerce/succes.php';

// $erreurPayement = '';


$checkout_session = \Stripe\Checkout\Session::create([
  'line_items' => [[
    'price_data' => [
      'product_data' => [
          'name' => 'Coder Proprement',
          'images' => ['https://static.fnac-static.com/multimedia/Images/FR/NR/73/22/a7/10953331/1540-1/tsp20230126082429/Coder-proprement.jpg'],
          "description" =>'LSi un code "sale" peut fonctionner, il peut également remettre en question la pérennité d\'une entreprise de développement de logiciels. Chaque année, du temps et des ressources sont gaspillés à cause d\'un code mal écrit...'],
      'currency' => 'eur',
      'unit_amount' => 2999,
  ],
  'quantity' => 1,
  ],
  
],
  'shipping_address_collection' => ['allowed_countries' => ['FR', 'IT', 'SN']],
  'mode' => 'payment',
  'success_url' => $urlPayementReussi . '?success=true',
  'cancel_url' => $urlPortfolio . '?canceled=true',
]);

header("HTTP/1.1 303 See Other");
header("Location: " . $checkout_session->url);