<?php
// 16:53 instale deja fais, def nako ndem nak
require_once '../cle.php';
require_once '../vendor/autoload.php';
require_once '../TransictionDonnees/InsertTransiction.php';


\Stripe\Stripe::setApiKey($stripeSecretKey);
header('Content-Type: application/json');

$urlPortfolio = 'http://localhost:3000';
$urlPayementReussi = 'http://backporftolio/ecommerce/succes.php';
// $erreurPayement = '';


$checkout_session = \Stripe\Checkout\Session::create([
  'line_items' => [[
    'price_data' => [
      'product_data' => [
          'name' => 'Apprenez les langages HTML5, CSS3 et JavaScript pour créer...',
          'images' => ['https://static.fnac-static.com/multimedia/Images/FR/NR/6e/b7/d9/14268270/1540-1/tsp20220425084351/HTML5-C3-et-JavaScript.jpg'],
          "description" =>'Ces deux livres offrent au lecteur un maximum d\'informations sur les langages JavaScript et HTML5 ainsi que sur les feuilles de style CSS3 pour développer des interfaces Front End dynamiques.'],
      'currency' => 'eur',
      'unit_amount' => 555999,
  ],
  'quantity' => 1,
  ],
  
],
  'shipping_address_collection' => ['allowed_countries' => ['FR', 'IT', 'SN']],
  'mode' => 'payment',
  'success_url' => $urlPayementReussi . '?success=true',
  'cancel_url' => $urlPortfolio . '?canceled=true',
]);

header("HTTP/1.1 303 See Other");
header("Location: " . $checkout_session->url);