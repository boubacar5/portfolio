<?php
// 16:53 instale deja fais, def nako ndem nak
require_once '../cle.php';
require_once '../vendor/autoload.php';
require_once '../TransictionDonnees/InsertTransiction.php';


\Stripe\Stripe::setApiKey($stripeSecretKey);
header('Content-Type: application/json');

$urlPortfolio = 'http://localhost:3000';
$urlPayementReussi = 'http://backporftolio/ecommerce/succes.php';
// $erreurPayement = '';


$checkout_session = \Stripe\Checkout\Session::create([
  'line_items' => [[
    'price_data' => [
      'product_data' => [
          'name' => 'Programmer Pour les Nuls 3ème édition...',
          'images' => ['https://static.fnac-static.com/multimedia/Images/FR/NR/c8/0a/84/8653512/1507-1/tsp20170607120540/Programmer-Pour-les-Nuls.jpg'],
          "description" =>'Avec Programmer pour les Nuls, il n\'est pas question de faire de vous un programmeur professionnel en quelques jours mais de vous mettre le pied à l\'étrier afin de vous apprendre à développer des programmes dans un langage structuré...'],
      'currency' => 'eur',
      'unit_amount' => 3999,
  ],
  'quantity' => 1,
  ],
  
],
  'shipping_address_collection' => ['allowed_countries' => ['FR', 'IT', 'SN']],
  'mode' => 'payment',
  'success_url' => $urlPayementReussi . '?success=true',
  'cancel_url' => $urlPortfolio . '?canceled=true',
]);

header("HTTP/1.1 303 See Other");
header("Location: " . $checkout_session->url);