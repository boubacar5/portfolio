-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 30 août 2023 à 08:30
-- Version du serveur : 5.7.40
-- Version de PHP : 8.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `portfolio`
--

-- --------------------------------------------------------

--
-- Structure de la table `achats`
--

DROP TABLE IF EXISTS `achats`;
CREATE TABLE IF NOT EXISTS `achats` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `client` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `adresse` varchar(250) NOT NULL,
  `etat` varchar(50) NOT NULL,
  `montant` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `payement_intent` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=164 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `achats`
--

INSERT INTO `achats` (`id`, `client`, `email`, `adresse`, `etat`, `montant`, `date`, `payement_intent`) VALUES
(61, 'Boubaa', 'gadiopapa99@gmail.com', '12 Rue de la RÃ©ole', 'succeeded', 1999, '2023-08-23 00:00:00', 'pi_3NiC5oAvhfr1At6W1vblnLkF'),
(158, 'Pool', 'boubacar.gadio.simplon@gmail.com', '12 Place du PanthÃ©on', 'succeeded', 1999, '2023-08-29 16:07:39', 'pi_3NkSVBAvhfr1At6W0J2iSreG'),
(159, 'Pool', 'boubacar.gadio.simplon@gmail.com', '12 Place du PanthÃ©on', 'succeeded', 1999, '2023-08-29 16:07:41', 'pi_3NkSVBAvhfr1At6W0J2iSreG'),
(76, 'Querra', 'boubacar.gadio.simplon@gmail.com', '34 Rue du Louvre', 'succeeded', 4999, '2023-08-23 00:00:00', 'pi_3NiE6qAvhfr1At6W1JfTToll'),
(65, 'Gadio', 'boubacar.gadio.simplon@gmail.com', '114 Rue du Mont Blanc', 'succeeded', 1999, '2023-08-23 00:00:00', 'pi_3NiCAnAvhfr1At6W0nQtTTzb'),
(88, 'Noapm', 'gadiopapa99@gmail.com', '55 Avenue Jean Mermoz', 'succeeded', 1999, '2023-08-24 00:00:00', 'pi_3NiYp0Avhfr1At6W0iUusim5'),
(89, 'Kolpm', 'boubacar.gadio.simplon@gmail.com', '12 Place de la Bastille', 'succeeded', 1999, '2023-08-24 00:00:00', 'pi_3NiYrxAvhfr1At6W1d9mLxb1'),
(90, 'PP', 'gadiopapa99@gmail.com', '55 Place Jules GrandclÃ©ment', 'succeeded', 1999, '2023-08-24 00:00:00', 'pi_3NiZRRAvhfr1At6W1NeZ4Hj6'),
(92, 'Querry Pok', 'gadiopapa99@gmail.com', '12 Avenue Montaigne', 'succeeded', 1999, '2023-08-24 00:00:00', 'pi_3NiZZmAvhfr1At6W1HNJ5Qwv'),
(93, 'Porrio', 'boubacar.gadio.simplon@gmail.com', '34 Place des Lices', 'succeeded', 2999, '2023-08-24 00:00:00', 'pi_3NiZd2Avhfr1At6W1cUe2TO3'),
(153, 'Polm', 'gadiopapa99@gmail.com', '12 Boulevard des Capucines', 'succeeded', 1999, '2023-08-25 10:49:53', 'pi_3NivAdAvhfr1At6W1jiKVX8K'),
(149, 'PolmZZ', 'gadiopapa99@gmail.com', '12 Rue Charles Mapou', 'succeeded', 2999, '2023-08-25 10:14:33', 'pi_3NiZtDAvhfr1At6W1VTNIHjr'),
(154, 'Cloalm', 'gadiopapa99@gmail.com', '12 Rue Philippe de Girard', 'succeeded', 1999, '2023-08-27 01:51:12', 'pi_3NjW7pAvhfr1At6W1SXGAnTW'),
(155, 'Table', 'gadiopapa99@gmail.com', 'gadiopapa99@gmail.com', 'succeeded', 1999, '2023-08-27 02:41:47', 'pi_3NjWHOAvhfr1At6W0CGSmkhs'),
(160, 'Pool', 'boubacar.gadio.simplon@gmail.com', '12 Place du PanthÃ©on', 'succeeded', 1999, '2023-08-29 16:11:06', 'pi_3NkSVBAvhfr1At6W0J2iSreG'),
(161, 'Pool', 'boubacar.gadio.simplon@gmail.com', '12 Place du PanthÃ©on', 'succeeded', 1999, '2023-08-29 16:15:14', 'pi_3NkSVBAvhfr1At6W0J2iSreG'),
(162, 'Pool', 'boubacar.gadio.simplon@gmail.com', '12 Place du PanthÃ©on', 'succeeded', 1999, '2023-08-29 16:15:41', 'pi_3NkSVBAvhfr1At6W0J2iSreG'),
(163, 'Pool', 'boubacar.gadio.simplon@gmail.com', '12 Place du PanthÃ©on', 'succeeded', 1999, '2023-08-29 16:16:45', 'pi_3NkSVBAvhfr1At6W0J2iSreG');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentaire` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`id`, `commentaire`) VALUES
(63, 'merceedes cd bsb'),
(61, 'dzde kekbz  '),
(60, 'coglione'),
(59, 'edze cezc'),
(58, 'ezde fnz fjnfr ez'),
(65, 'shh  dzdz '),
(54, 'dzed ld,poe nnze fnzef b ze'),
(64, 'bello davvereo d'),
(52, 'beau bouloo t kebdiezb ub izeub ');

-- --------------------------------------------------------

--
-- Structure de la table `contacter`
--

DROP TABLE IF EXISTS `contacter`;
CREATE TABLE IF NOT EXISTS `contacter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contacter`
--

INSERT INTO `contacter` (`id`, `message`, `nom`, `prenom`, `email`) VALUES
(6, 'dezd oinozd on dubzed iun zeezne iznbfz onoe znin dun duibda ubdaondea in,ad,d inoda niodiaz dnida ondanz ionda oinak d', 'efez', 'dzdde', 'boubacar.gadio.simplon@gmail.com'),
(8, 'polmon dpi doznodz odannd odoiznda odzdzan dppjdjazdzodaodz oiznda dziadnz zidzand ', 'Poll', 'Poolm', 'boubacar.gadio.simplon@gmail.com'),
(9, 'salutt le monde!', 'hhh', 'hhh', 'boubacar.gadio.simplon@gmail.com'),
(10, 'nagua deff', 'pok', 'polm', 'boubacar.gadio.simplon@gmail.com'),
(11, 'Pok polm snin ana', 'pok', 'Polm', 'boubacar.gadio.simplon@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `jaime`
--

DROP TABLE IF EXISTS `jaime`;
CREATE TABLE IF NOT EXISTS `jaime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jaime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `jaime`
--

INSERT INTO `jaime` (`id`, `jaime`) VALUES
(1, 483);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `typeUtilisateur` varchar(50) NOT NULL DEFAULT 'utilisateur',
  `dernierConnexion` datetime NOT NULL,
  `jeton` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id_utilisateur`, `nom`, `prenom`, `email`, `password`, `typeUtilisateur`, `dernierConnexion`, `jeton`) VALUES
(126, 'gadio', 'Papa', 'boubacar.gadio.simplon@gmail.com', '$2y$10$6U/6ZoAKglDMg4939Oa67.amxLWsHDr0mRbAzc.zCiSBDrqPHhkLO', 'utilisateur', '2023-08-24 09:44:17', NULL),
(127, 'zaz', 'Mopni', 'ee@ee', '$2y$10$cAkAkbyHGw73uji0fhleb.7LdxjW279jTKy55rxrWWYCsQZdWLcWW', 'utilisateur', '2023-08-23 00:00:00', NULL),
(128, 'sas', 'Rollo', 'aa@aa', '$2y$10$jTgkrN2BNSPUA67vuxGQDeihPd6LCBfu6fEopdJArpu055trbl.Gq', 'utilisateur', '2023-08-26 02:14:44', '943495ad14b4ca4b62aafcf8bdb0a573'),
(129, 'aqa', 'Moppo', 'qq@qq', '$2y$10$RBuNF0rS7xicGWVf/llYLuFuSwWRq.G55As5idAh6N0iv5hkbIEn6', 'utilisateur', '2023-08-24 10:32:44', NULL),
(130, 'pok', 'Polm', 'polm@polm', '$2y$10$DBd5ZraiDGtnazBO1qTuB.PrIctlmXOkj2K6.58vDdQ5uMYZg7aW6', 'utilisateur', '2023-08-23 00:00:00', NULL),
(131, 'Polm', 'Jolio', 'jolio@jolio', '$2y$10$0GTdjzOQf.ntI12E4tI1heK6rXoWTlnNO4qw2KSLXaybMjo2MgIHe', 'utilisateur', '2023-08-24 11:27:21', NULL),
(132, 'Gadio', 'Gadio', 'admin@admin', '$2y$10$R51OYQyYwZTZKw30a221FexsYSVK0aQdiLYL69UI70EZbuGcp6w96', 'admin', '2023-08-23 00:00:00', NULL),
(133, 'polm', 'pok', 'sass@sass', '$2y$10$TjfD73Tdc8spOKRZi9JS1upgwaW.HsQ1ata6XpwGj2hTQNzN0O8.O', 'utilisateur', '2023-08-23 00:00:00', NULL),
(134, 'deze', 'dez', 'beso@beso', '$2y$10$iWBEKQqa54ndllEfIU.i0.R.2KLaVACQ2rzYNXBDbAe1mD2c6DAd6', 'utilisateur', '2023-08-23 00:00:00', NULL),
(135, 'pok', 'querry', 'pp@pp', '$2y$10$qXdQd8AKRFVh33SI1dWu4eIRQD8xsFaL1NVwD1JP9IHjdeyekX6Fm', 'utilisateur', '2023-08-28 16:48:03', 'd41fbee29d59e4201eed39b0586fb358'),
(136, 'baba', 'boubacar', 'pm@pm', '$2y$10$SnWYjvuz2.134Imrv.7uzONIswKnG0W.NtDQHS8KbtqS2D.nyigL.', 'utilisateur', '2023-08-24 09:43:42', NULL),
(137, 'szsz', 'ppl', 'azerty@azerty', '$2y$10$rQaIxh2FE3gzvbKZ4mE3j.e.FgFpJHdn0iqkgl8l/8H8255jRGkXS', 'utilisateur', '2023-08-28 16:52:45', NULL),
(138, 'pp', 'polm', 'boubacar.gadio.simplon@gmail.comss', '$2y$10$zgtSKiHNQbF8yAs8DjNSdepu9/LayX0mfpPPn7jzB7s7IFyYo.Bgq', 'utilisateur', '2023-08-29 15:50:22', NULL),
(139, 'po', 'selim', 'selim@gmail.com', '$2y$10$BuS/ByUvGOHnYSddeiNy7.5r5QlnMRpFByHE4tuIBaSA5hhmoorcS', 'utilisateur', '2023-08-29 16:00:26', NULL),
(140, 'PP', 'POL', 'selim@gmail.comfef', '$2y$10$P7KiG7ES3yCq5v2z62v7aedj96NGu4scVdv6YP2DUm/tV.3snTprq', 'utilisateur', '2023-08-29 16:03:45', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
