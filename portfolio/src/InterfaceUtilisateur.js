import React from 'react'
import Header from './components/Header'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import Apropos from './Pages/Apropos';
import Competences from './Pages/Competance';
import Portfolio from './Pages/Portfolio';
import Achats from './Pages/Achats';
import Contact from './Pages/Contact';
import Footer from './components/footer';
import Collaborations from './Pages/Collaborations';



function Interfaceutilisateur() {
  return (
      <div id='InterfaceUtilisateur'>
        <Header/>
        <Apropos/>
        <Competences/>
        <Portfolio/>
        <Achats/>
        <Collaborations/>
        <Contact/>
        <Footer/> 
      </div>
      

  )
}



export default Interfaceutilisateur;

