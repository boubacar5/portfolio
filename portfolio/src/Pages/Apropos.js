import React, {useRef, useState} from "react";
import axios from "axios";
import emailjs from '@emailjs/browser';
import logoPortfolio from '../logoPortfolio.svg';
import dev from '../ImgPortfolio/software-developer.jpg'
import line from '../ImgPortfolio/line.mp4'


const Apropos = () =>{
    const prenom = localStorage.getItem('prenom');

    const formulaire = useRef();

    const [messageP, setMessageP] = useState(false);
    const togglemessageP = () => {
        setMessageP(!messageP);
    };

    const [data, setData] = useState({
        nom: "",
        prenom: "",
        email: "",
        message: "",
      });
    
      // evenement handleChange
    const handleChange = (e) => {
        setData({ ...data, [e.target.id]: e.target.value });
      };
    const submitMessage = (e) => {
      e.preventDefault();
  
      const sendData = {
        nom: data.nom,
        prenom: data.prenom,
        email: data.email,
        message: data.message,
      };
      console.log(sendData);
  
      if (data.nom.trim() === '' || data.prenom.trim() === '' || data.email.trim() === '' || data.message.trim() === '') {
        alert('Veuillez remplir tous les champs.');
        return;
      }
  
      axios
        .post('http://backporftolio/Contact/Contact.php', sendData)
        .then(function (response) {
          if (!data.status == 'valid') {
            alert('Erreur message');
          } else {
             // inviamento in email
            emailjs.sendForm('service_4o2tyq3', 'template_ghmqitg', formulaire.current, 'akIZc5s4hHJDQC0Yq')
            .then((result) => {
                console.log(result.text);
            }, (error) => {
                console.log(error.text);
            });
            alert('Message bien envoye');
            window.location.reload();
          }
          console.log(response);
        })
        .catch(function (error) {
          console.log(error);
        });
        
       
  
      };
    return (
        <div>
                <video src={line} type="video/mp4" autoPlay loop muted></video>
            <div className="presentation p-5">
                <span>Salut {prenom},</span> <br/>
                    {/* <span>Salut ,</span> <br/> */}
                    <span>C'est <img src={logoPortfolio}></img>oubacar</span><br/>
                    <span>Bienvenue sur mon site</span><br/>
                    
                    <button className="m-5 mecontacter" onClick={togglemessageP}>Me Contacter!</button>
            </div>
            <div className="p-5 profil" id="Apropos">
                <h1 >Mon Profil Personnel</h1>
                <div className="textimg">
                <p className="">Je suis un développeur web passionné, ayant effectué mes études secondaires en droit et économie en Italie avant de m'installer à Grenoble. C'est là que j'ai découvert le développement web, un domaine qui a instantanément captivé mon intérêt. Actuellement, je me forme activement dans le domaine du développement web.
                <br></br> <br></br>     
                Grâce à ma formation en cours et à ma passion constante pour le domaine, je suis continuellement à la recherche de nouvelles opportunités pour développer mes compétences et relever des défis stimulants. Je suis également ouvert à la collaboration avec d'autres professionnels pour créer des projets uniques et innovants.
                <br></br> <br></br>  
                N'hésitez pas à explorer mon portfolio pour découvrir mes projets précédents et à me contacter si vous souhaitez discuter de collaborations ou de possibilités professionnelles.
                </p>
                <div id="Aprops">
                    <img src={dev} className="" ></img>
                </div>
                </div>
            </div>
            {messageP && (
                  <div className="connexionP">
                    <div onClick={togglemessageP} className="overlay"></div>
                    <div className="formulaireContact">
                      <h1>Page de contact</h1>

                      <form className="" ref={formulaire} onSubmit={submitMessage}>
                        <div class="formulaire">
                            <label for="prenom">Prénom:</label>
                            <input type="text" id="prenom" name="prenom" onChange={handleChange} value={data.prenom}></input>
                        </div>
                        <div class="formulaire">
                            <label for="nom">Nom:</label>
                            <input type="text" id="nom" name="nom" onChange={handleChange} value={data.nom}></input>
                        </div>
                        <div class="formulaire">
                            <label for="email">Email:</label>
                            <input type="email" id="email" name="email" onChange={handleChange} value={data.email}></input>
                        </div>
                        <div class="formulaire">
                            <label for="message">Message:</label>
                            <textarea id="message" name="message" onChange={handleChange} value={data.message}></textarea>
                        </div>
                        <div class="formulaire">
                            <button type="submit">Envoyer</button>
                        </div>
                    </form>

                      <button onClick={togglemessageP} className="xFormulaire">
                        <i className="bi bi-x-lg"></i>
                      </button>
                    </div>
                  </div>
                )}
        </div >
    )
}

export default Apropos