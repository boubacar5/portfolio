import React from "react";
import apprendreHtmlcssP from '../ImgPortfolio/apprendreHtmlcssP.jpg';
import programmerpournulP from '../ImgPortfolio/programmerpournulP.jpg';
import codeproprementP from '../ImgPortfolio/codeproprementP.jpg';
import phpsymfonyP from '../ImgPortfolio/phpsymfonyP.jpg';
import devfullP from '../ImgPortfolio/devfullP.png'
import axios from "axios";

const  Achats = () =>{

    // Dans votre composant Achats
    const achats = () => {
        axios.post('http://backporftolio/ecommerce/Payment/Paylivre1.php')
        .then((response) => {
            const jeton = response.data.jeton;
    
            // Rediriger vers le site de paiement Stripe avec le jeton
            window.location.href = `https://checkout.stripe.com/c/pay/cs_test_a1s8JdgENsc4aLg16sRfuG2PuawUmJgySKgy9NXVwKOVe5YszFNmZm8ZZl#fidkdWxOYHwnPyd1blpxYHZxWjA0SFAxX2JEc21jdzREcTNSUzNAa2lMbVdgUVd%2FYXNBVEkwbWQ0Ml80b1xRZndfaj0yXFVXYHVNTn1CT2BIf0w1PH19NjJzU2dxSWBwVWNxQk9TSD0wQ1NHNTVDYXJKbzRKSycpJ2N3amhWYHdzYHcnP3F3cGApJ2lkfGpwcVF8dWAnPyd2bGtiaWBabHFgaCcpJ2BrZGdpYFVpZGZgbWppYWB3dic%2FcXdwYHgl${jeton}`;
        })
        .catch(function (error) {
            console.log(error);
        });
    };
    
    return(
        <div className="achats p-5" id="Achats">
            <h1>Achats des livres</h1>
            <div className="achatLivre">
            <div className="livres">
            
                <div className="livre">
                    <img src={apprendreHtmlcssP}></img>
                    <span className="p-3"><h4>Apprenez les langages HTML5, CSS3 et JavaScript pour créer...</h4>
                    {/* <p>Le livre présente dans un premier temps le langage HTML5, permettant de structurer une page, de disposer les éléments visuels très précisément comme le fait un logiciel de mise en page ou de dessin...</p>  */}
                    <form action='http://backporftolio/ecommerce/Payment/Paylivre1.php' onClick={achats} method="POST">
                        <button type="submit">Acheter a 19,99€</button>
                    </form>
                    </span>
                </div>
                <div className="livre">
                    <img src={codeproprementP}></img>
                    <span className="p-3"><h4>Apprenez les langages HTML5, CSS3 et JavaScript pour créer...</h4>
                    {/* <p>Si un code "sale" peut fonctionner, il peut également remettre en question la pérennité d'une entreprise de développement de logiciels. Chaque année, du temps et des ressources sont gaspillés à cause d'un code mal écrit...</p>  */}
                    <form action='http://backporftolio/ecommerce/Payment/Paylivre2.php' method="POST">
                        <button type="submit">Acheter a 29,99€</button>
                    </form>
                    </span>
                </div>
                <div className="livre">
                    <img src={programmerpournulP}></img>
                    <span className="p-3"><h4>Programmer pour les Nuls grand format, 3e édition</h4>
                    {/* <p>Avec Programmer pour les Nuls, il n'est pas question de faire de vous un programmeur professionnel en quelques jours mais de vous mettre le pied à l'étrier afin de vous apprendre à développer des programmes dans un langage structuré...</p>  */}
                    <form action='http://backporftolio/ecommerce/Payment/Paylivre3.php' method="POST">
                        <button type="submit">Acheter a 39,99€</button>
                    </form>
                    </span>
                </div>
            
                <div className="livre">
                    <img src={phpsymfonyP}></img>
                    <span className="p-3"><h4>Apprendre à développer des applications web avec PHP et Symfony</h4>
                    {/* <p>Ce livre s'adresse à toute personne qui souhaite disposer des connaissances nécessaires pour apprendre à développer des applications web avec PHP et le framework Symfony (en version 5 au moment de l'écriture).</p>  */}
                    <form action='http://backporftolio/ecommerce/Payment/Paylivre4.php' method="POST">
                        <button type="submit">Acheter a 49,99€</button>
                    </form>
                    </span>
                </div>
                <div className="livre">
                    <img src={devfullP}></img>
                    <span className="p-3"><h4>Apprenez les langages HTML5, CSS3 et JavaScript pour créer...</h4>
                    {/* <p>Ces deux livres offrent au lecteur un maximum d'informations sur les langages JavaScript et HTML5 ainsi que sur les feuilles de style CSS3 pour développer des interfaces Front End dynamiques.</p>  */}
                    <form action='http://backporftolio/ecommerce/Payment/Paylivre5.php' method="POST">
                        <button type="submit">Acheter a 59,99€</button>
                    </form>
                    </span>                
                </div>
            </div>
            </div>
        </div>
    )

}

export default Achats