import React, { useState, useEffect } from 'react';
import axios from 'axios';


const Portflio = () =>{
    const [projets, setProjets] = useState([]);

    useEffect(() =>{
        getProjets();
    }, []);

    function getProjets(){
        axios.get('http://backporftolio/Donnees/Projets.php').then(function(response){
            console.log(response.data);
            setProjets(response.data)
        })
        .catch(error => {
            console.error(error);
        });
    }
    return (
        <div className="portfolio p-5" id="Portfolio">
        <h1>Porfolio</h1>
            <p className="">Voici quelques uns de mes projets que j'ai réalisés durant ma formation</p>
            <div className="projet">
                <div className="cartes">

                    {projets.map((data, key) => (<div className="carte" >
                        <img src={data.lienImage}></img>
                        <a href={data.lienGit} target="_blank"><span>{data.titre}</span></a>
                    </div>))}
                    
                </div>
            </div>
        </div>
    )
}

export default Portflio