import React, { useState, useRef } from "react";
import axios from "axios";
import emailjs from '@emailjs/browser';

const Contact = () =>{
    
  const formulaire = useRef();

  const [data, setData] = useState({
      nom: "",
      prenom: "",
      email: "",
      message: "",
    });
  
    // evenement handleChange
  const handleChange = (e) => {
      setData({ ...data, [e.target.id]: e.target.value });
    };
  const submitMessage = (e) => {
    e.preventDefault();

    const sendData = {
      nom: data.nom,
      prenom: data.prenom,
      email: data.email,
      message: data.message,
    };
    console.log(sendData);

    if (data.nom.trim() === '' || data.prenom.trim() === '' || data.email.trim() === '' || data.message.trim() === '') {
      alert('Veuillez remplir tous les champs.');
      return;
    }

    axios
      .post('http://backporftolio/Contact/Contact.php', sendData)
      .then(function (response) {
        if (response.data.status === 'valid') {
          emailjs.sendForm('service_4o2tyq3', 'template_ghmqitg', formulaire.current, 'akIZc5s4hHJDQC0Yq')
          .then((result) => {
              console.log(result.text);
          }, (error) => {
              console.log(error.text);
          });
    
          alert('Message bien envoye');
          window.location.reload();
         } else if (response.data.status === 'emailInvalide'){
          alert ("L'email  est invalider!")
        }
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
      

    };
    return (
        <div className="contacter" id="Contact">
            <h1 className="p-5">Me Contacter</h1>
            <p className="">Si vous souhaitez discuter d'un projet ou si vous avez simplement une question, veuillez remplir le formulaire ci-dessous.</p>

            <div className="contactemaps">
            <form className="" ref={formulaire} onSubmit={submitMessage}>
                <div class="formulaire">
                    <label for="prenom">Prénom:</label>
                    <input type="text" id="prenom" name="prenom" onChange={handleChange} value={data.prenom}></input>
                </div>
                <div class="formulaire">
                    <label for="nom">Nom:</label>
                    <input type="text" id="nom" name="nom" onChange={handleChange} value={data.nom}></input>
                </div>
                <div class="formulaire">
                    <label for="email">Email:</label>
                    <input type="email" id="email" name="email" onChange={handleChange} value={data.email}></input>
                </div>
                <div class="formulaire">
                    <label for="message">Message:</label>
                    <textarea id="message" name="message" onChange={handleChange} value={data.message}></textarea>
                </div>
                <div class="formulaire">
                    <button type="submit">Envoyer</button>
                </div>
            </form>
                <div className="">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d90029.36474956086!2d5.59368574336!3d45.15839190000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478af4c81af5f247%3A0xcd4b076ccd487696!2sTridenTT%20Travail%20Temporaire%20%3A%20Travail%20temporaire%20%C3%A0%20Grenoble!5e0!3m2!1sfr!2sfr!4v1685715929705!5m2!1sfr!2sfr"  style={{ border: "0" }} allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade" ></iframe>
                </div>
            </div>
        </div>
    )
}

export default Contact
