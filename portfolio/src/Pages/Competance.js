import React, {useEffect, useState} from "react";
import axios from 'axios'

const Competences = () => {


    const [experiences, setExperiences] = useState([]);

    useEffect(() =>{
        getExperiences()
    }, []);

    function getExperiences(){
        axios.get('http://backporftolio/Donnees/Experiences.php').then(function(response){
            console.log(response.data);
            setExperiences(response.data)
        })
         .catch(error => {
            console.error(error);
        });
    }
    return (
        <div className="competence p-5" id="Compentence">
                <h1 >Experiences & <br></br>
                    Competences
                </h1>
                <div className=" textLigne">

                    <div>
                    {experiences.map((data, key) =>(
                    <div className="textExperiences" key={key}>
                        <h5>{data.titre}</h5>
                        <h6>{data.date}</h6>
                        <p>{data.paragraphe}</p>
                    </div>
                    ))}
                    </div>

                <div className="ligneCompetence"> 
                        <h4>HTML/CSS</h4> 
                    <div className="d-flex">
                        <div className="ligne1"></div>
                            <div className="lignea"></div>
                    </div> <br></br>
                        <h4>JavaScript/React</h4> 
                    <div className="d-flex">
                        <div className="ligne2"></div>
                            <div className="ligneb"></div>
                    </div><br></br>
                    <h4>Bootstrap</h4> 
                    <div className="d-flex">
                        <div className="ligne3"></div>
                            <div className="lignec"></div>
                    </div><br></br>
                    <h4>PHP/Symfony</h4> 
                    <div className="d-flex">
                        <div className="ligne4"></div>
                            <div className="lignee"></div>
                    </div><br></br>
                    <h4>MySQL</h4> 
                    <div className="d-flex">
                        <div className="ligne5"></div>
                            <div className="lignef"></div>
                    </div><br></br>
                    <h4>Django</h4> 
                    <div className="d-flex">
                        <div className="ligne6"></div>
                            <div className="ligneg"></div>
                    </div>

                    <div className="utilisation">
                        <h4>
                            Utilisations
                        </h4>
                        <ul>
                            <li> Git </li>
                            <li> Docker</li>
                            <li> Trello/Notion</li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
    )
}

export default Competences