import React, { useEffect, useState } from "react";
import axios from "axios";



const Transiction =()=>{

    const [data, setData] = useState([]);
    useEffect(() => {
        getData();
    }, []);

    function getData(){
        axios.get('http://backporftolio/ecommerce/TransictionDonnees/SelectTransiction.php').then(function(response){
            console.log(response.data);
            setData(response.data)
        })
    }

    
    const supprimerData = (id) => {
        axios.delete("http://backporftolio/ecommerce/TransictionDonnees/SupprimerTransiction.php", {data: {id: id}})
        .then((response) => {
            getData(response);
        })
    }

    return(
        <div id='Achats'>
        <h1>Liste des Achats</h1> <br></br>
    
        <div className="table-responsive text-center">
            <table className="table table-striped">
                <thead>
                    <tr>
                        {/* <th>ID</th> */}
                        <th>Client</th>
                        <th>Email</th>
                        <th>Adresse</th>
                        <th>État</th>
                        <th>Montant</th>
                        <th>Date</th>
                        <th>Paiement Intent</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((data, key) => (
                        <tr key={key}>
                            {/* <td>{data.id}</td> */}
                            <td>{data.client}</td>
                            <td>{data.email}</td>
                            <td>{data.adresse}</td>
                            <td>{data.etat}</td>
                            <td>{data.montant}</td>
                            <td>{data.date}</td>
                            <td>{data.payement_intent}</td>
                            <td >
                                <i className="bi bi-trash" onClick={() => supprimerData(data.id)}></i>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    </div>
    
    )

}

export default Transiction