import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

const DernierConnexion = () =>{

    const [data, setData] = useState([])
    useEffect(() => {
        getData();
    }, [])

    function getData(){
        axios.get('http://backporftolio/Connexion/DernierConnexion')
        .then(function (response){
            setData(response.data);
        })
    }

    return(
        <div id='DernierConnexion'>
            <h1>Liste des derniers connecter</h1> <br></br>
            
            <div className="table-responsive text-center">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Prenom et Nom</th>
                            <th>Email</th>
                            <th>Date et Heure</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((data, key) => (  
                            <tr key={key}>
                                <td>{data.id_utilisateur}</td>
                                <td>{data.prenom} {data.nom}</td>
                                <td>{data.email}</td>
                                <td>{data.dernierConnexion}</td>
                                <td> <Link to={`${data.id_utilisateur}/ModifierUtilisateurs`}>Modifier</Link></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default DernierConnexion