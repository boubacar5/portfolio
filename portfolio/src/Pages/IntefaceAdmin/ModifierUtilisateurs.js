import React, { useState, useEffect } from 'react';
import axios from 'axios'
import { useParams } from 'react-router-dom';

function ModifierUtilisateurs() {

    const [inputs, setInputs] = useState([])

    const {id} = useParams();

    useEffect(() =>{
        getUtilisateurs()
    }, [])

    function getUtilisateurs(){
        axios.get(`http://backporftolio/Connexion/ModifierUtilisateurs/${id}`)
        .then(function(response){
            console.log(response.data)
        })
    }


    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values =>({...values, [name]:value}));
    }

    // const handleSubmit = (event) => {
    //     event.preventDefault();

    //     axios.post('', inputs)
    //     .then(function(response){
    //         console.log(response.data)
    //     })
    // };

    return (
        <div className="container">
            <h1>Modifier Utilisateur</h1>
            <form >
                <div className="mb-3">
                    <label htmlFor="nom" className="form-label">Modifier Nom:</label>
                    <input type="text" className="form-control" id="nom" name="nom" 
                        onChange={handleChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="prenom" className="form-label">Modifier Prénom:</label>
                    <input type="text" className="form-control" id="prenom" name="prenom" 
                        onChange={handleChange}
                    />
                </div>
                <div className="mb-3">
                    <label htmlFor="email" className="form-label">Modifier Email:</label>
                    <input type="email" className="form-control" id="email" name="email" 
                        onChange={handleChange}
                    />
                </div>
                <button type="submit" className="btn btn-primary">Modifier</button>
            </form>
        </div>
    );
}

export default ModifierUtilisateurs;
