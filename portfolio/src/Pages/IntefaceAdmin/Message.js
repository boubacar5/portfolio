import React, { useEffect, useState } from "react";
import axios from "axios";


const Message  = () =>{

    const [data, setData] = useState([]);
    useEffect(() => {
        getData();
    }, []);

    function getData(){
        axios.get('http://backporftolio/Contact/Message.php').then(function(response){
            console.log(response.data);
            setData(response.data)
        })
    }

    
    const supprimerData = (id) => {
        axios.delete("http://backporftolio/Contact/SupprimerMessage.php", {data: {id: id}})
        .then((response) => {
            getData(response);
        })
    }
    

    return(
        <div id='Message'>
            <h1>Liste des Messages</h1> <br></br>
            
            <div className="table-responsive text-center">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Destinataire</th>
                            <th>Email</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((data, key) => (
                            <tr key={key}>
                                <td>{data.prenom} {data.nom}</td>
                                <td>{data.email}</td>
                                <td>{data.message}</td>
                                <td >
                                    <i className="bi bi-trash" onClick={() => supprimerData(data.id)}></i>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>

    )
}
export default Message