import React, { useState } from "react";
import axios from 'axios'

const Projet = () =>{

const [data, setData] = useState({
    titre: "",
    lienImage: "",
    lienGit: "",
  });

  // evenement handleChange
  const handleChange = (e) => {
    setData({ ...data, [e.target.id]: e.target.value });
  };

  const handleProjet = (e) => {
    e.preventDefault();

    const sendData = {
      titre: data.titre,
      lienImage: data.lienImage,
      lienGit: data.lienGit,
    };
    console.log(sendData);

    if (data.titre.trim() === '' || data.lienImage.trim() === '' || data.lienGit.trim() === '') {
      alert('Veuillez remplir tous les champs.');
      return;
    }

    axios
      .post('http://backporftolio/Donnees/AjouterProjets.php', sendData)
      .then((response) => {
        console.log(response);
        if (response.data.status ==='valide'){
            alert('Projet Ajouter!')
        }
      })
      .catch(function (error) {
        console.log(error);
        alert("Une erreur s'est produite, veuillez réessayer!")
      });
     
  };

    return(
        <div id="AjouterProjets">
            <h1>Ajouter des projets</h1>

            <div className="table-responsive text-center">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Titre</th>
                            <th>Lien Image</th>
                            <th>Lien Git</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><input id="titre" name="titre" onChange={handleChange} value={data.titre} /></td>
                            <td><input id="lienImage" name="lienImage" onChange={handleChange} value={data.lienImage} /></td>
                            <td><input id="lienGit" name="lienGit" onChange={handleChange} value={data.lienGit} /></td>
                            <td type="submit" onClick={handleProjet}>Enrigistre</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Projet