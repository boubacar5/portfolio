import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp as solidThumbsUp } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp as regularThumbsUp } from '@fortawesome/free-regular-svg-icons';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import { faAngleUp, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import apple from '../ImgPortfolio/apple.jpg';
import google from '../ImgPortfolio/google.jpg';
import tesla from '../ImgPortfolio/tesla.jpg';

const Collaborations = () => {  
    
    const [data, setData] = useState([]); 
    const [jaimes, setJaimes] = useState([]); 

    useEffect(() => {
        getData();
        getJaimes();

        const intervalId = setInterval(getData, 10000);
        setInterval(getJaimes, 10000)
        return () => clearInterval(intervalId);
    }, []);
   

    function getData(){
        axios.get('http://backporftolio/Collaborations/voirCommentaire.php').then(function(response){
            console.log(response.data);
            setData(response.data);
            
        })
    }
    function getJaimes(){
        axios.get('http://backporftolio/Collaborations/voirLike.php').then(function(res){
            console.log(res.data);
            setJaimes(res.data);
        })
        .catch(error => {
            console.error(error);
        });
    }
    

    const [text1P, setText1P] = useState(true);
    const handleApple = () => {
        setText1P(!text1P);
        setText2P(false);
        setText3P(false);       
    };
    const [text2P, setText2P] = useState(false);
    const handleGoogle = () => {
        setText2P(!text2P);
        setText1P(false);
        setText3P(false);
    }; 
    const [text3P, setText3P] = useState(false);
    const handleTesla = () => {
        setText3P(!text3P);
        setText1P(false);
        setText2P(false);
    };

    
    const [commentaire, setCommentaire] = useState(false);
    const [voirCommentaire, setVoirCommentaire] = useState(false);
    const toggleVoirCommentaire = () => {
        setVoirCommentaire(!voirCommentaire);
        setCommentaire(!commentaire)
        
    };


    const [liked, setLiked] = useState(false);
    const handleLike = () => {
        axios.get('http://backporftolio/Collaborations/like.php')
            .then((response) => {
                if (response.data.status === 'liker') {
                    alert('Merci pour votre Like :)')
                    setLiked(true);
                    // window.reload()
                } 
            })
            .catch(function(error) {
                console.error(error);
            });
    }

    const handleChange = (e) => {
        setData({ ...data, [e.target.id]: e.target.value }); 
    };

    const handleCommentaire = (e) => {
        e.preventDefault();

        const sendData = { commentaire: data.commentaire };
        console.log(sendData);

        axios.post('http://backporftolio/Collaborations/commentaire.php', sendData)
            .then((response) => {
                if (response.data.status === 'valide') {
                    alert('Merci pour votre commentaire :)')
                    // window.location.reload()
                }
            })
            .catch(function(error) {
                console.error(error);
            });
    }

    console.log(jaimes);
    return (
        <div id='Collaborations'>
            <h1>Mes Collaborations</h1>

            <div>
                <img src={apple} onClick={handleApple} alt='Apple'/>
                <img src={google} onClick={handleGoogle} alt='Google'/>
                <img src={tesla} onClick={handleTesla} alt='Tesla'/>
            </div>

            <div>
                {text1P &&(
                    <div className='collabText'>
                    <span>Collaborations avec Apple</span>
                        <p>Je suis ravi de partager ma collaboration passionnante avec Apple ! En travaillant main dans la main avec leur équipe d'ingénieurs talentueux, nous avons conçu et développé une application innovante qui révolutionne la façon dont les utilisateurs interagissent avec leur environnement numérique. Grâce aux dernières technologies d'Apple, notre application offre une expérience utilisateur fluide, des fonctionnalités avancées et une esthétique élégante. Cette collaboration a été une opportunité unique d'apprendre, de grandir et de contribuer à l'écosystème Apple. J'ai hâte de voir notre application atteindre des millions d'utilisateurs et de continuer à repousser les limites de ce qui est possible dans le monde de la technologie.</p>
                    </div>
                )}
                
                 {text2P &&(
                    <div className='collabText'>
                    <span>Collaboration avec Google</span>
                        <p>C'est avec une grande fierté que je partage mon expérience de collaboration avec Google. Ensemble, nous avons créé une solution numérique novatrice qui résout des problèmes complexes et offre des fonctionnalités de pointe. En utilisant les technologies avancées de Google, nous avons pu repousser les limites de la performance, de la sécurité et de l'innovation. Travailler en étroite collaboration avec les experts de Google m'a permis d'acquérir une compréhension approfondie des meilleures pratiques en matière de développement et de concevoir des produits qui apportent une valeur réelle aux utilisateurs du monde entier. Cette collaboration avec Google restera un moment marquant de ma carrière de développeur, et je suis impatient de voir les impacts positifs que notre travail aura sur la technologie et la société.</p>
                    </div>
                )}
                 {text3P &&(
                    <div className='collabText'>
                    <span>Collaboration avec Tesla</span>
                        <p>C'est avec une immense joie que je partage ma collaboration passionnante avec Tesla. Travailler aux côtés d'une entreprise visionnaire comme Tesla a été une expérience incroyablement enrichissante. En tant que développeur, j'ai eu la chance de contribuer à la conception et au développement de technologies innovantes pour les véhicules électriques de Tesla. Ensemble, nous avons repensé la façon dont les conducteurs interagissent avec leurs voitures, en créant des fonctionnalités intelligentes et connectées qui redéfinissent l'expérience de conduite. La collaboration avec les esprits brillants chez Tesla m'a inspiré à repousser les limites de la technologie et à créer des produits qui façonnent l'avenir de la mobilité durable. Je suis honoré d'avoir fait partie de cette aventure passionnante et j'ai hâte de voir notre impact sur le monde.</p>
                    </div>
                )}
            </div>
            <div>
                    <div className='nombreJaime'>
                    <span onClick={handleLike}>
                        {liked ? <FontAwesomeIcon icon={solidThumbsUp} className='jaime'/> : <FontAwesomeIcon  icon={regularThumbsUp} className='jaime' />}
                    </span>
                        {jaimes.map((item, key) => (
                            <p key={item.id}>{item.jaime}</p>
                        ))}
                    </div>
                <div className='commentInput'>
                    <input className='commenter' type="text" placeholder='Commentez!' id="commentaire" name="commentaire" onChange={handleChange} value={data.commentaire}/>
                    <FontAwesomeIcon icon={faPaperPlane} onClick={handleCommentaire} type='submit'  />
                </div>
            </div>
            <div>
                <p className='voir' onClick={toggleVoirCommentaire}>Voir les autres commentaires </p> <FontAwesomeIcon onClick={toggleVoirCommentaire} className='iconVoir' icon={voirCommentaire ? faAngleUp : faAngleDown} />
            </div>
            <div className='listCommentaire'>

                {commentaire && (
                        <ul>
                            {data.map((data, key) => (
                                <li key={key}>{data.commentaire}</li>
                            ))}
                        </ul>
                    )}
            </div>

        </div>
    );
};

export default Collaborations;
