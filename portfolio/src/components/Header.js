import React, { useState, useEffect } from "react";
import axios from 'axios';
import logoPortfolio from '../logoPortfolio.svg';
import 'bootstrap-icons/font/bootstrap-icons.css';
import { MdClose } from 'react-icons/md';
import { FiMenu } from 'react-icons/fi';

import { Anchor } from 'antd';
const { Link } = Anchor;


function Header() {

  const [connexionP, setConnexionP] = useState(false);
  const toggleConnexionP = () => {
    setConnexionP(!connexionP);
  };

  const [inscriptionP, setInscriptionP] = useState(false);
  const toggleInscriptionP = () => {
    setInscriptionP(!inscriptionP);
  };

  const [isConnected, setIsConnected] = useState(false);

  // Récupérer l'état de connexion depuis le localStorage
  useEffect(() => {
    const isConnectedInLocalStorage = window.localStorage.getItem('isConnected');
    
    if (isConnectedInLocalStorage === 'true') {
      setIsConnected(true);
    }
  }, []);

  const [data, setData] = useState({
    nom: "",
    prenom: "",
    email: "",
    password: "",
  });

  // evenement handleChange
  const handleChange = (e) => {
    setData({ ...data, [e.target.id]: e.target.value });
  };

  const submitInscription = (e) => {
    e.preventDefault();

    const sendData = {
      nom: data.nom,
      prenom: data.prenom,
      email: data.email,
      password: data.password,
    };
    console.log(sendData);

    if (data.nom.trim() === '' || data.prenom.trim() === '' || data.email.trim() === '' || data.password.trim() === '') {
      alert('Veuillez remplir tous les champs.');
      return;
    }

    axios
      .post('http://backporftolio/Connexion/Inscription.php', sendData)
      .then((response) => {
        console.log(response);
        if (response.data.status=='valide'){

          alert("Inscription reussi!");
          setInscriptionP(!inscriptionP);
          window.location.href = 'http://localhost:3000/?prenom=' + encodeURIComponent(data.prenom);
          // pour recupere le prenom d'utilisateur connecter
          localStorage.setItem('prenom', data.prenom);
          window.localStorage.setItem('isConnected', 'true');
          window.location.reload();

        } else if (response.data.status=='invalide') {

          alert("L'email fourni a déjà été utilisé, veuillez réessayer avec une autre! ");  
                 
        } else if (response.data.status=='emailInvalide'){

          alert('email insere est pas valider')

        }
      })
      .catch(function (error) {
        console.log(error);
        alert("Une erreur s'est produite lors de l'inscription, veuillez réessayer!")
      });
     
  };


  const submitConnexion = (e) => {
    e.preventDefault();

    const sendData = {
      email: data.email,
      password: data.password,
    };
    console.log(sendData);

    if (data.email.trim() === '' || data.password.trim() === '') {
      alert('Veuillez remplir tous les champs.');
      return;
    }
       
    axios.post('http://backporftolio/Connexion/Connexion.php', sendData)
      .then((response) => {
        if (response.data.Status==='utilisateur'){

          window.localStorage.setItem('email', response.data.email);
          window.localStorage.setItem('prenom', response.data.prenom);
          window.location.href = 'http://localhost:3000/?prenom=' + encodeURIComponent(data.prenom);
          setConnexionP(!connexionP);
          setIsConnected(true);
          window.localStorage.setItem('isConnected', 'true');
          window.location.reload();

        } else if (response.data.Status==='admin'){

          window.location.href = 'http://localhost:3000/InterfaceAdmin';
          setConnexionP(!connexionP);
          setIsConnected(true);
          window.localStorage.setItem('isConnected', 'true');
          alert('Page admin');
          window.location.reload();
          
        } else {
            alert('Email ou Mot de Passe invalide');
        }
      })  
      .catch(function (error) {
        console.log(error);
      });
  };


  const handleDeconnexion = () => {
    axios.get('http://backporftolio/Connexion/Deconnexion.php')
      .then((response) => {
        if (response.data.status==='deconnecter'){
          window.localStorage.clear(); 
          window.location.href = 'http://localhost:3000/';
          window.localStorage.removeItem('isConnected');
          window.location.href = 'http://localhost:3000/';
          // setIsConnected(false);
          // window.location.reload();
          // console.log(response)
        } 
      })
      .catch(function (error) {
        console.log(error);
      });
    }

  const [navbarOpen, setNavbarOpen] = useState(false);
  const toggleNavbar = () => {
    setNavbarOpen(!navbarOpen);
  };

  const closeNavbar = () => {
    setNavbarOpen(false);
  };

  const [question, setQuestion ] = useState(true)
  const handleQuestion = () =>{
    setQuestion(false);
  }

  
  return (
    <Anchor>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
        <img src={logoPortfolio} alt="logo" className="d-flex justify-content-start"></img>
          <button
            className="navbar-toggler"
            onClick={() => setNavbarOpen((prev) => !prev)}
          >
            {navbarOpen ? (
              <MdClose style={{ width: '28px', height: '28px' }} />
            ) : (
              <FiMenu
                style={{
                  width: '28px',
                  height: '28px',
                }}
              />
            )}
          </button>

          <div className={`collapse navbar-collapse justify-content-center ${navbarOpen ? 'show' : ''}`} id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link className="nav-link" href="#Apropos" title="A Propos" onClick={closeNavbar} />
              </li>
              <li className="nav-item">
                <Link className="nav-link" href="#Compentence" title="Competences" onClick={closeNavbar} />
              </li>
              <li className="nav-item">
                <Link className="nav-link" href="#Portfolio" title="Portfolio" onClick={closeNavbar} />
              </li>
              <li className="nav-item">
                <Link className="nav-link" href="#Achats" title="Achats" onClick={closeNavbar} />
              </li>
              <li className="nav-item">
                <Link className="nav-link" href="#Collaborations" title="Collaboration" onClick={closeNavbar} />
              </li>
              <li className="nav-item">
                <Link className="nav-link" href="#Contact" title="Contact" onClick={closeNavbar} />
              </li>
            </ul>
          </div>
          <div className={`collapse navbar-collapse justify-content-end connexion ${navbarOpen ? 'show' : ''}`} id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
              <button className="nav-link connexion" onClick={isConnected ? handleDeconnexion : toggleConnexionP}>
                <i className="bi bi-person-fill"></i> {isConnected ? "Déconnexion" : "Connexion"}
               </button>

                {/* connexion ou login */}
                {/* {question && (
                   <div >
                    <div onClick={handleQuestion} className="overlay"></div>
                    <div className="question">
                      <h1>Connexion</h1>
                      <h1>Inscription</h1>
                      <h1>juste pour voir</h1>
                    <button onClick={handleQuestion} className="xFormulaire">
                        <i className="bi bi-x-lg"></i>
                      </button>
                    </div>
                  </div>
                )} */}

                {/* popup */}
                {connexionP && (
                  <div className="connexionP">
                    <div onClick={toggleConnexionP} className="overlay"></div>
                    <div className="formulaireConnexion">
                      <h1>Page Connexion</h1>
                      <form onSubmit={submitConnexion}>
                        <div className="formulaire">
                          <label htmlFor="Email">Email :</label>
                          <input type="Email" id="email" name="email" onChange={handleChange} value={data.email}></input>
                        </div>
                        <div className="formulaire">
                          <label htmlFor="password">Mot de Passe :</label>
                          <input type="password" id="password" name="password" onChange={handleChange} value={data.password}></input>
                        </div>
                      <div className="formulaire">
                        <button type="submit">Envoyer</button>
                      </div>
                      </form>
                      <button onClick={toggleConnexionP} className="xFormulaire">
                        <i className="bi bi-x-lg"></i>
                      </button>
                    </div>
                  </div>
                )}
              </li>
              <li className="nav-item">
                <button className="nav-link inscription" onClick={toggleInscriptionP}>
                  <i className="bi bi-box-arrow-in-right"></i> Inscription
                </button>


                {/* popup */}
                {inscriptionP && (
                  <div className="connexionP">
                    <div onClick={toggleInscriptionP} className="overlay"></div>
                    <div className="formulaireConnexion">
                      <h1>Page d'Inscription</h1>

                      <form onSubmit={submitInscription} >
                        <div className="formulaire">
                          <label htmlFor="prenom">Prénom :</label>
                          <input type="text" id="prenom" name="prenom" onChange={handleChange} value={data.prenom} />
                        </div>
                        <div className="formulaire">
                          <label htmlFor="nom">Nom :</label>
                          <input id="nom" name="nom" onChange={handleChange} value={data.nom} />
                        </div>
                        <div className="formulaire">
                          <label htmlFor="Email">Email :</label>
                          <input type="Email" id="email" name="email" onChange={handleChange} value={data.email} />
                        </div>
                        <div className="formulaire">
                          <label htmlFor="password">Mot de Passe :</label>
                          <input type="password" id="password" name="password"onChange={handleChange} value={data.password}/>
                        </div>
                        <div className="formulaire">
                          <button type="submit" onSubmit={submitInscription}>Envoyer</button>
                        </div>
                      </form>
                      <button onClick={toggleInscriptionP} className="xFormulaire">
                        <i className="bi bi-x-lg"></i>
                      </button>
                    </div>
                  </div>
                )}
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </Anchor>
  );
}

export default Header;
