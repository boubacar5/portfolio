import React from 'react'

const Footer = () =>{
    return(
        <footer className='text-center'>
                Boubacar Gadio | &copy;copyright 2023<br></br>
                boubacar.gadio.simplon@gmail.com
                
        </footer>
    )
}

export default Footer