import React from 'react'
import Header from './components/Header'
import Message from './Pages/IntefaceAdmin/Message';
import Transiction from './Pages/IntefaceAdmin/Transiction';
import Utilisateurs from './Pages/IntefaceAdmin/Utilisateurs';
import Apropos from './Pages/Apropos';
import Competences from './Pages/Competance';
import Portfolio from './Pages/Portfolio';
import Achats from './Pages/Achats';
import Contact from './Pages/Contact';
import Footer from './components/footer';
import Collaborations from './Pages/Collaborations';
import Projet from './Pages/IntefaceAdmin/Projet';





function InterfaceAdmin() {
  return (
    
      <div id="InterfaceAdmin">
        <Header/>
        <Projet/>
        <Message/>
        <Transiction/>
        <Utilisateurs/>
        <Competences/>
        <Portfolio/>
        <Achats/>
        <Collaborations/>
        <Contact/>
   
      </div>

  )
}



export default InterfaceAdmin;

