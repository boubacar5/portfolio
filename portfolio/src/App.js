import React from 'react';
import { BrowserRouter as Router, Routes, Route, BrowserRouter } from 'react-router-dom';
import InterfaceUtilisateur from './InterfaceUtilisateur'
import InterfaceAdmin from './InterfaceAdmin';
import ModifierUtilisateurs from './Pages/IntefaceAdmin/ModifierUtilisateurs'


function App() {
  return (
    <Router>
      <Routes> 
          <Route path="/" element={<InterfaceUtilisateur/>}/>
          <Route path="/InterfaceAdmin" element={<InterfaceAdmin/>}/>
          <Route path="/InterfaceAdmin/:id/ModifierUtilisateurs" element={<ModifierUtilisateurs/>}/>
      </Routes>
    </Router>
  );
}

export default App;
